# Webscope Coding Challenge

## Setup 

`npm install` to get set up

`gulp watch` to watch & compile

This repo has a simple sass setup which we resue on all our projects. We use Bootstrap 4 and try to make use of the utiltiy classes provided by Bootstrap as much as possible. https://getbootstrap.com/docs/4.0/utilities/borders/

We also have a number of custom utility classes included that we reuse on most projects. 

## Task 

We have put together a simple one page design of a todo app, which we would like you to implement, to demonstrate your knowledge and skills.

All html can be static and placed within build/index.html

Please ensure that your implementation is responsive.

Please fork this repo and push your work to that, then provide the link to your repo.

Don't worry about cross browser compatability for this project, we'll be checking your submission using the latest version of Chrome, so feel free to use the latest css features.

Any questions email tom@webscope.co.nz